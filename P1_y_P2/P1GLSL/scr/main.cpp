/****************************/
/* G3D - Daniel Camba Lamas */
/****************************/

/*** DEFINES ************************/
#define GLM_FORCE_RADIANS
#define Deg2Rad (3.14159f / 180.0f)
/***********************************/

/*** INCLUDES **************************/
#include "BOX.h"
#include <iostream>
#include <IGL/IGlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
/***************************************/

// VARS
float near = 1.0f;
float far = 10.0f;
float FOV = tan( 60 * Deg2Rad ); // A bigger scalar, a bigger FOV
// For rotation by mouse
float mouseX, mouseY;

// MATRIX
glm::mat4 view = glm::mat4( 1.0f );
glm::mat4 proj = glm::mat4( 0.0f );
glm::mat4 modl = glm::mat4( 1.0f );
// Camera fix
float yaw, pitch;
glm::vec3 eyeVec = glm::vec3( 0 );

// Scene objects
int objId = -1;
int objId2 = -1;

// CallBacks
void UpdateView();
void resizeFunc( int width, int height );
void idleFunc();
void keyboardFunc( unsigned char key, int x, int y );
void mouseFunc( int button, int state, int x, int y );
void mouseMotionFunc( int x, int y );

// --- ENTRY POINT ---
int main( int argc, char** argv )
{
    std::locale::global( std::locale( "spanish" ) );// acentos ;)

    bool init = false;

    // P1.1 Aspect ratio  -> void resizeFunc( int width, int height )
    // P1.2 Second object -> void idleFunc()
    // P1.3 Move camera   -> (WASD = Move) (IJKL = Rotate)
    //init = IGlib::init( "../shaders_P1/shader.v7.vert", "../shaders_P1/shader.v7.frag" );

    // P2.1 -> Two lights
    //init = IGlib::init( "../shaders_P2/shader.v2.vert", "../shaders_P2/shader.v3.frag" ); // P2.1 + P2.Opt_2

    // P2.2 -> P2.1 + Attenuation
    //init = IGlib::init( "../shaders_P2/shader.v2.vert", "../shaders_P2/shader.v4.frag" ); // P2.2 + P2.Opt_2

    // P2.3 -> P2.2 + Spot and Directional
    init = IGlib::init( "../shaders_P2/shader.v2.vert", "../shaders_P2/shader.v5.frag" ); // P2.3 + P2.Opt_2

    // Break if not files
    if ( !init )
        return -1;

    //CBs
    IGlib::setResizeCB( resizeFunc );
    IGlib::setIdleCB( idleFunc );
    IGlib::setKeyboardCB( keyboardFunc );
    IGlib::setMouseCB( mouseFunc );
    IGlib::setMouseMoveCB( mouseMotionFunc );

    // Project matrix settings
    proj[ 0 ].x = 1.0f / FOV;
    proj[ 1 ].y = proj[ 0 ].x;
    proj[ 2 ].z = -( far + near ) / ( far - near );
    proj[ 2 ].w = -1.0f;
    proj[ 3 ].z = -2 * near*far / ( far - near );
    IGlib::setProjMat( proj );

    // --- OBJECTS

    // Obj1
    objId = IGlib::createObj( cubeNTriangleIndex,
                              cubeNVertex,
                              cubeTriangleIndex,
                              cubeVertexPos,
                              cubeVertexColor,
                              cubeVertexNormal,
                              cubeVertexTexCoord,
                              cubeVertexTangent );
    IGlib::setModelMat( objId, modl );
    IGlib::addColorTex( objId, "../img/color2.png" );
    IGlib::addSpecularTex( objId, "../img/specMap.png" );
    IGlib::addEmissiveTex( objId, "../img/emissive.png" );

    //Obj2
    objId2 = IGlib::createObj( cubeNTriangleIndex,
                               cubeNVertex,
                               cubeTriangleIndex,
                               cubeVertexPos,
                               cubeVertexColor,
                               cubeVertexNormal,
                               cubeVertexTexCoord,
                               cubeVertexTangent );
    IGlib::setModelMat( objId2, modl );
    IGlib::addColorTex( objId2, "../img/color2.png" );
    IGlib::addSpecularTex( objId2, "../img/specMap.png" );
    IGlib::addEmissiveTex( objId2, "../img/emissive.png" );

    // --- END OBJECTS

    // --- Mainloop ---
    UpdateView();
    IGlib::mainLoop();
    IGlib::destroy();
    return 0;
}

// RESIZE WINDOW ACTIONS - [ P1.1 ]
void resizeFunc( int width, int height )
{
    float ratio = (float) width / (float) height;
    proj[ 0 ].x = 1.0f / ( FOV * ratio ); // This is a correction to FOV and object position.
    IGlib::setProjMat( proj ); // Apply changes
}

// IDLE STATE ACTIONS
void idleFunc()
{
    static float angle;
    angle = ( angle > 2.0f * 3.14159 ) ? 0.0f : angle + 0.01f;

    // First object behaviour
    modl = glm::mat4( 1.0f );
    modl = glm::rotate( modl, angle, glm::vec3( 0.0, 1.0, 0.0 ) );
    IGlib::setModelMat( objId, modl );

    // Second object behaviour - [ P1.2 ]
    modl = glm::mat4( 1.0f );
    modl = glm::rotate( modl, angle, glm::vec3( 0.0f, 0.0f, 1.0f ) );  //Arround first cube ( vertical anticlockwise )
    modl = glm::translate( modl, glm::vec3( 3.0f, 0.0f, 0.0f ) );      //Move away the first cube
    modl = glm::rotate( modl, angle, glm::vec3( 0.0f, 1.0f, 0.0f ) );  //Over himself
    IGlib::setModelMat( objId2, modl );
}

// KEYBOAD INPUT ACTIONS
void keyboardFunc( unsigned char key, int x, int y )
{
    std::cout << "KEY: " << key << std::endl << std::endl;
    const float speed = 0.15f;//how fast we move
    float dx = 0; //how much we strafe on x
    float dz = 0; //how much we walk on z
    // Evaluate input key - [ P1.3 ]
    switch ( key )
    {
        // Movement
    case 'w':
        dz = speed;
        break;
    case 's':
        dz = -speed;
        break;
    case 'a':
        dx = speed;
        break;
    case 'd':
        dx = -speed;
        break;
        // Camera
    case 'l':
        pitch += speed / 2;
        break;
    case 'j':
        pitch -= speed / 2;
        break;
    case 'k':
        yaw += speed / 2;
        break;
    case 'i':
        yaw -= speed / 2;
    default:
        break;
    }

    glm::vec3 forward( view[ 0 ][ 2 ], view[ 1 ][ 2 ], view[ 2 ][ 2 ] ); // Z
    glm::vec3 strafe( view[ 0 ][ 0 ], view[ 1 ][ 0 ], view[ 2 ][ 0 ] ); // X
    eyeVec += ( dz * forward + dx * strafe );
    UpdateView();
}

// MOUSE BUTTONS INPUT ACTIONS
void mouseFunc( int button, int state, int x, int y )
{
    if ( state == 0 ) { std::cout << "MOUSE BUTTON PRESSED: "; } else { std::cout << "MOUSE BUTTON RELEASED: "; }
    if ( button == 0 ) std::cout << "Left" << std::endl;
    if ( button == 1 ) std::cout << "Center" << std::endl;
    if ( button == 2 ) std::cout << "Right" << std::endl;
    std::cout << "AT (" << x << ", " << y << ")" << std::endl << std::endl;

    if ( state == 0 && button == 0 ) // left click enter
    {
        mouseX = (float) x;
        mouseY = (float) y;
    }
}

// MOUSE DISPLACEMENT INPUT ACTIONS - [ P1.O1 ]
void mouseMotionFunc( int x, int y )
{
    pitch += ( mouseX - (float) x ) * 0.0001f;
    yaw += ( mouseY - (float) y ) * 0.0001f;
    UpdateView();
}

// UPDATE CORRECTLY - Fix camera bug of lose Z axis
void UpdateView()
{
    glm::mat4 matYaw = glm::mat4( 1.0f );   //identity matrix
    glm::mat4 matPitch = glm::mat4( 1.0f ); //identity matrix

    // Apply acumulated angles
    matYaw = glm::rotate( matYaw, yaw, glm::vec3( -1.0f, 0.0f, 0.0f ) );
    matPitch = glm::rotate( matPitch, pitch, glm::vec3( 0.0f, 1.0f, 0.0f ) );

    // Calc roration
    glm::mat4 rotate = glm::mat4( 1.0f );
    rotate = matYaw * matPitch;

    // Calc translation
    glm::mat4 translate = glm::mat4( 1.0f );
    translate[ 3 ].z = -6;
    translate = glm::translate( translate, -eyeVec );

    // New view will be the product of two previous matrix
    view = rotate * translate;
    IGlib::setViewMat( view );
}