#version 330 core
out vec4 outColor;
in vec3 norm;
in vec3 color;
in vec3 position;
vec3 Il = vec3(1);
vec3 pl = vec3(0);
vec3 Ia = vec3(0.15);
vec3 N;
vec3 pos;
float n = 5;
vec3 Ka = vec3(1,0,0);
vec3 Kd = vec3(1,0,0);
vec3 Ks = vec3(1);
vec3 shade()
{
	vec3 c = vec3(0);
	c += Ia * Ka;
	vec3 L = normalize(pl-pos);
	c += clamp(Il * Kd * dot(N,L), 0, 1);
	vec3 R = reflect(-L,N);
	vec3 V = normalize(-pos);
	float spcFactor = pow(max(dot(R,V),0.00001),n);
	c += Il * Ks * spcFactor;
	return c;
}
void main()
{
	N = normalize(norm);
	pos = position;
	Ka = color;
	Kd = color;
	outColor = vec4(shade(), 1.0);
}
