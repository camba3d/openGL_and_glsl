#version 330 core
uniform sampler2D colorTex;
uniform sampler2D specularTex;
uniform sampler2D emiTex;
out vec4 outColor;
in vec3 norm;
in vec3 position;
in vec3 color;
in vec2 texCoord;
vec3 Il = vec3(1);
vec3 pl = vec3(0);
vec3 Ia = vec3(0.15);
vec3 N;
vec3 pos;
float n = 5;
vec3 Ka = vec3(1,0,0);
vec3 Kd = vec3(1,0,0);
vec3 Ks = vec3(1);
vec3 Ke = vec3(0);

vec3 shade()
{
	vec3 c = vec3(0);
	c += Ia * Ka;
	vec3 L = normalize(pl-pos);
	c += clamp(Il * Kd * dot(N,L), 0, 1);
	vec3 R = reflect(-L,N);
	vec3 V = normalize(-pos);
	float spcFactor = pow(max(dot(R,V),0.00001),n);
	c += Il * Ks * spcFactor;
	c += Ke;
	return c;
}

void main()
{
	N = normalize(norm);
	pos = position;
	Ka = texture(colorTex,texCoord).rgb;
	Kd = Ka;
	Ks = texture(specularTex,texCoord).rgb;
	Ke = texture(emiTex,texCoord).rgb;
	vec3 I = shade();
	vec3 cf = vec3(0);
	float d = length(pos);
	float alfa = 1/exp(0.02*d*d);
	vec3 c = alfa*I+(1-alfa)*cf;
	outColor = vec4(c,1.0);
}
