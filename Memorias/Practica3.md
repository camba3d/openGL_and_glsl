# Practica 4

> Daniel Camba Lamas

## Obligatoria

### 1. Modifica las propiedades (intensidad y posición) de la luz a través del teclado

La intensidad será una variable float que modificaremos con las teclas '+' y '-'.

La posición será un vector de 3floats que modificaremos con el numPad usando:

- 4 y 6 para izquierda y derecha (X)
- 8 y 2 para subir y bajar (Y)
- 5 y 0 para acercar y alejar (Z)

Luego dichas variables se envian como uniform al shader.

```C++
// intensidad de luz
if (u_Ia_Value != -1)
    glProgramUniform1f(program, u_Ia_Value, Ia_Value);

// posicion de luz
if (u_lpos_Value != -1)
    glProgramUniform3fv(program, u_lpos_Value, 1, lpos_Value);
```

### 2. Define una matriz de proyección que conserve el aspect ratio

Modificando la función de redimensionado de forma que tenga en cuenta el nuevo ancho y alto de la ventana.

```C++
void resizeFunc(int width, int height) {
    glViewport(0, 0, width, height);
    float ratio = (float)width / (float)height;
    proj = glm::perspective(glm::radians(60.0f), ratio, 0.1f, 50.0f);
}
```

### 3. Añade un nuevo cubo a la escena

Dado que queremos pintar el mismo cubo, pintamos el VAO con otra matriz model.

De forma que en la función render modificamos la matriz model aplicandole las transformaciones que queremos y
volvemos llamar a la función de pintado.

### 4. Utiliza un programa diferente para cada cubo

Necesitamos por lo tanto otra pareja de shaders por lo que modificamos la función *initShader*:

```C++
void initShader(const char *vname, const char *fname, const char *vname2, const char *fname2)
```

Para que acepte dos shaders mas y asigne estos a un nuevo programa y creamos nuevas variables globales (se ha usado el mismo nombre que para las del primer programa pero con un *2* después del nombre)

Luego en la función de render, hacemos que el segundo objeto utilice el nuevo programa, usando:

```C++
glUseProgram(program2);
```

Para hacer apreciable la diferencia en tiempo de ejecución, la luz en este shader no es modificada por teclado, en los del primer programa sí, además el otro cubo usará los shaders sin texturas.

### 5. Control de la cámara con el teclado

Las teclas IJKL moverán la camara de manera intuitiva, utilizando los conceptos de Yaw y Pitch, construyendo la matriz vista en cada modificación del angulo o desplazamiento (utilizando WASD) de forma que no se pierde en ningún momento el eje Z.

## Optativa

### 2. Controla el giro de la cámara utilizando el ratón

Ya que la matriz vista se calcula de nuevo tras cada modificación, lo unico que se debe hacer es modificar el Yaw y el Pitch para afectar a la rotación en Y y en X respectivamente.

Para ello:

```C++
// Gracias a esto capturamos el click inicial
void mouseFunc(int button, int state, int x, int y) {
  if (state == 0) {
    std::cout << "MOUSE BUTTON PRESSED: ";
  } else {
    std::cout << "MOUSE BUTTON RELEASED: ";
  }
  if (button == 0)
    std::cout << "Left" << std::endl;
  if (button == 1)
    std::cout << "Center" << std::endl;
  if (button == 2)
    std::cout << "Right" << std::endl;
  std::cout << "AT (" << x << ", " << y << ")" << std::endl << std::endl;

  if (state == 0 && button == 0) // left click enter
  {
    mouseX = (float)x;
    mouseY = (float)y;
  }
}

// Gracias a esto calculamos el desplazamiento del ratón
void mouseMotionFunc(int x, int y) {
  pitch += (mouseX - (float)x) * 0.0001f;
  yaw += (mouseY - (float)y) * 0.0001f;
  UpdateView();
}
```
