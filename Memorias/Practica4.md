# Practica 4

> Daniel Camba Lamas

## Obligatoria

**\* Clamp2**:

``` C++
template <typename t>
t clamp2(t x, t min, t max) {
    if (x < min) x = min;
    if (x > max) x = max;
    return x;
}
```

### 1. Motion blur por teclado

- ROJO:
  - 'R' incrementa
  - 'r' decrementa
- AZUL:
  - 'B' incrementa
  - 'b' decrementa
- VERDE:
  - 'G' incrementa
  - 'g' decrementa
- ALPHA:
  - 'P' incrementa
  - 'p' decrementa
- Activar/Desactivar = '0'

Los parametros tienen aplicado un **clamp2** entre 0.f y 1.f

### 2. Controla los parámetros del DOF por teclado

- Distancia focal:
  - 'F' incrementa
  - 'f' decrementa

Este parametro tiene aplicado un **clamp2** entre -30.f y 0.f

- Distancia de desenfoque máximo:
  - 'M' incrementa
  - 'm' decrementa

Este parametro tiene aplicado un **clamp2** entre 1.f y 10.f

### 3. Utiliza el buffer de profundidad para controlar el DOF

- Activar/Desactivar = 'c' o 'C'

**En cliente**:

Se envia una variable uniform que actuará de flag para decidir si el DOF se define por la distancia de la camara o por los valores por teclado.

```C++
if (u_dofByCam != -1)
    glProgramUniform1i(postProccesProgram, u_dofByCam, dofByCam);
else
    std::cout << "WARNING: u_dofByCam no definida" << std::endl;
```

**En el shader**:

Comprobamos como calcular el DOF:

```C++
float dof = 0.0;
if (dofByCam){
    dof = dofCam();
} else {
    dof = dofKb(focalDistance);
}
```

Donde las funciones dofCam y dofKb son:

```C++
// DOF via keyboard-set parameters.
float dofKb(float fDist) {
    float dof = abs(texture(vertexTex,texCoord).z - fDist) * maxDistanceFactor;
    dof = clamp (dof, 0.0, 1.0);
    dof *= dof;
    return dof;
}

// DOF via camera distance.
float dofCam() {
    float zCam =  near*far / (far + (texture(vertexTex,texCoord).z * (near-far)));
    return dofKb(zCam);
}
```

### 4. Mascaras de convolución mediante Uniforms

Se han definido matrices de convolución (al principio del fichero junto las demás variables) que definen distintos efectos y son activables por teclado.

- '1' = Desenfoque (por defecto)
- '2' = Detectar bordes
- '3' = Repujado
- '4' = Enfatizado del enfoque

Las mascaras se definen como un array de 25floats y son enviadas al shader de la siguiente manera:

```C++
if (u_mask != -1)
    glProgramUniform1fv(postProccesProgram, u_mask, 25, mask[mask_index]);
else
    std::cout << "WARNING: u_mask no definida" << std::endl;
```

Donde *mask[mask_index]* devuelve un puntero a la matriz de convolución correspondiente y *25* es el numero de floats que lleva dicha matriz.

## Optativa

### 2. Concatena varios post-procesos distintos

Si se pulsa la tecla '0' estarán activos a la vez el filtro gausiano activo y el motion blur.

### 4.b Controla el giro de camara utilizando el ratón

Tambien puede moverse por teclado con las teclas IJKL.
Se ha añadido a glut la funcion que maneja el giro de camara por ratón.

```C++
glutMotionFunc(mouseMotionFunc);
```

El movimiento de la cámara sucede de forma que en ningún momento se pierde el eje Z por lo que el desplazamiento WASD siempre será natural.
