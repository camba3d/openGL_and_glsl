#version 330 core

//Color de salida
out vec4 outColor;

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;



void main()
{
//C�digo del Shader
outColor = vec4(texture(colorTex, texCoord).xyz, 0.6);
//outColor = vec4(texCoord,vec2(1.0));

//outColor = texelFetch (colorTex, gl_FragCoord.xy,0)


}