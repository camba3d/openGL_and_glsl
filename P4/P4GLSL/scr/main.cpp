﻿#include "BOX.h"
#include "auxiliar.h"
#include "PLANE.h"

#include <windows.h>

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h>
#include <iostream>
#define GLM_FORCE_RADIANS
#include <algorithm>
#include <cstdlib>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

#define RAND_SEED 31415926
#define SCREEN_SIZE 500, 500

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

// Matrices
glm::mat4 proj = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

// Variables para los ejercicios
float yaw;
float pitch;
glm::vec3 eyeVec = glm::vec3(0);
float RED = 0.5f;
float GREEN = 0.5f;
float BLUE = 0.5f;
float ALPHA = 0.7f;
bool DOF_by_camera = false;
int dofByCam = 0;
float _NEAR = 1.0f;
float _FAR = 50.0f;
float mouseX;
float mouseY;
bool blendActive = false;

// MATRICES DE CONVOLUCION
int mask_index = 0;
// Desenfoque
float mf_1 = (1.f / 65.f);
float mask1[25] = {
	1.0f * mf_1, 2.0f * mf_1, 3.0f * mf_1, 2.0f * mf_1, 1.0f * mf_1,
	2.0f * mf_1, 3.0f * mf_1, 4.0f * mf_1, 3.0f * mf_1, 2.0f * mf_1,
	3.0f * mf_1, 4.0f * mf_1, 5.0f * mf_1, 4.0f * mf_1, 3.0f * mf_1,
	2.0f * mf_1, 3.0f * mf_1, 4.0f * mf_1, 3.0f * mf_1, 2.0f * mf_1,
	1.0f * mf_1, 2.0f * mf_1, 3.0f * mf_1, 2.0f * mf_1, 1.0f * mf_1 };
// Detectar bordes
float mf_2 = 1.f;
float mask2[25] = {
	0.0f * mf_2, 0.0f * mf_2,  0.0f * mf_2,  0.0f * mf_2, 0.0f * mf_2,
	0.0f * mf_2, 0.0f * mf_2,  1.0f * mf_2,  0.0f * mf_2, 0.0f * mf_2,
	0.0f * mf_2, 1.0f * mf_2, -4.0f * mf_2, 1.0f * mf_2, 0.0f * mf_2,
	0.0f * mf_2, 0.0f * mf_2,  1.0f * mf_2,  0.0f * mf_2, 0.0f * mf_2,
	0.0f * mf_2, 0.0f * mf_2,  0.0f * mf_2,  0.0f * mf_2, 0.0f * mf_2 };
// Repujado
float mf_3 = 1.f;
float mask3[25] = {
	0.0f * mf_3,  0.0f * mf_3,  0.0f * mf_3,  0.0f * mf_3, 0.0f * mf_3,
	0.0f * mf_3, -2.0f * mf_3, -1.0f * mf_3,  0.0f * mf_3, 0.0f * mf_3,
	0.0f * mf_3, -1.0f * mf_3,  1.0f * mf_3,  1.0f * mf_3, 0.0f * mf_3,
	0.0f * mf_3,  0.0f * mf_3,  1.0f * mf_3,  2.0f * mf_3, 0.0f * mf_3,
	0.0f * mf_3,  0.0f * mf_3,  0.0f * mf_3,  0.0f * mf_3, 0.0f * mf_3 };
// Enfatizar enfoque
float mf_4 = 1.f;
float mask4[25] = {
	0.0f * mf_4,  0.0f * mf_4,  0.0f * mf_4,  0.0f * mf_4, 0.0f * mf_4,
	0.0f * mf_4,  0.0f * mf_4, -1.0f * mf_4,  0.0f * mf_4, 0.0f * mf_4,
	0.0f * mf_4, -1.0f * mf_4,  5.0f * mf_4, -1.0f * mf_4, 0.0f * mf_4,
	0.0f * mf_4,  0.0f * mf_4, -1.0f * mf_4,  0.0f * mf_4, 0.0f * mf_4,
	0.0f * mf_4,  0.0f * mf_4,  0.0f * mf_4,  0.0f * mf_4, 0.0f * mf_4 };
// Default
float *mask[] = { mask1, mask2, mask3, mask4 };

// uniforms (ID servidor)
unsigned int u_mask;
unsigned int u_dofByCam;
unsigned int u_focal_distance;
unsigned int u_max_distance_factor;
// valores para las uniform (cliente)
float focalDistance = -20.0f;
float auxFocalDistance = focalDistance;
float maxDistance = 3.0f;
float maxDistanceFactor = 1.0f / maxDistance;

//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
float angle = 0.0f;

// VAO
unsigned int vao;

// VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

unsigned int colorTexId;
unsigned int emiTexId;

// Por definir
unsigned int vshader;
unsigned int fshader;
unsigned int program;

// Variables Uniform
int uModelViewMat;
int uModelViewProjMat;
int uNormalMat;

// Texturas Uniform
int uColorTex;
int uEmiTex;

// Atributos
int inPos;
int inColor;
int inNormal;
int inTexCoord;

//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////

// Declaración de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);

void renderCube();

// Funciones de inicialización y destrucción
void initContext(int argc, char **argv);
void initOGL();
void initShaderFw(const char *vname, const char *fname);
void initObj();
void destroy();

// Carga el shader indicado, devuele el ID del shader
//! Por implementar
GLuint loadShader(const char *fileName, GLenum type);

// Crea una textura, la configura, la sube a OpenGL,
// y devuelve el identificador de la textura
//!!Por implementar
unsigned int loadTex(const char *fileName);

//////////////////////////////////////////////////////////////
// Nuevas variables auxiliares
//////////////////////////////////////////////////////////////
unsigned int postProccesVShader;
unsigned int postProccesFShader;
unsigned int postProccesProgram;
// Uniform
unsigned int uColorTexPP;
// Atributos
int inPosPP;

unsigned int planeVAO;
unsigned int planeVertexVBO;

unsigned int fbo;
unsigned int colorBuffTexId;
unsigned int depthBuffTexId;

unsigned int uVertexTexPP;
unsigned int vertexBuffTexId;


//////////////////////////////////////////////////////////////
// Nuevas funciones auxiliares
//////////////////////////////////////////////////////////////
void initShaderPP(const char *vname, const char *fname);
void initPlane();
void initFBO();
void resizeFBO(unsigned int w, unsigned int h);

// Para los ejercicios
void UpdateView();
float getFocalDistByCamera();
void mouseMotionFunc(int x, int y);
template <typename t> t clamp2(t x, t min, t max);

////////////// ENTRYPOINT ////////////
int main(int argc, char **argv) {
	std::locale::global(std::locale("spanish")); // acentos ;)

	initContext(argc, argv);
	initOGL();
	initShaderFw("../shaders_P4/fwRendering.v1.vert",
		"../shaders_P4/fwRendering.v1.frag");
	initShaderPP("../shaders_P4/postProcessing.v1.vert",
		"../shaders_P4/postProcessing.v1.frag");
	initObj();
	initPlane();
	initFBO();
	resizeFBO(SCREEN_SIZE);

	glutMainLoop();

	destroy();

	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares
void initContext(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	// glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(SCREEN_SIZE);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Prácticas GLSL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion
		<< std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
}

void initOGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, _NEAR, _FAR);
	view = glm::mat4(1.0f);
	view[3].z = -25.0f;
}

void destroy() {
	glDetachShader(program, vshader);
	glDetachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);
	glDeleteProgram(program);

	glDetachShader(postProccesProgram, postProccesVShader);
	glDetachShader(postProccesProgram, postProccesFShader);
	glDeleteShader(postProccesVShader);
	glDeleteShader(postProccesFShader);
	glDeleteProgram(postProccesProgram);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (inPos != -1)
		glDeleteBuffers(1, &posVBO);
	if (inColor != -1)
		glDeleteBuffers(1, &colorVBO);
	if (inNormal != -1)
		glDeleteBuffers(1, &normalVBO);
	if (inTexCoord != -1)
		glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);

	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &colorTexId);
	glDeleteTextures(1, &emiTexId);
}

void initShaderFw(const char *vname, const char *fname) {
	vshader = loadShader(vname, GL_VERTEX_SHADER);
	fshader = loadShader(fname, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);

	glBindAttribLocation(program, 0, "inPos");
	glBindAttribLocation(program, 1, "inColor");
	glBindAttribLocation(program, 2, "inNormal");
	glBindAttribLocation(program, 3, "inTexCoord");

	glLinkProgram(program);

	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);

		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;

		glDeleteProgram(program);
		program = 0;
		exit(-1);
	}

	uNormalMat = glGetUniformLocation(program, "normal");
	uModelViewMat = glGetUniformLocation(program, "modelView");
	uModelViewProjMat = glGetUniformLocation(program, "modelViewProj");

	uColorTex = glGetUniformLocation(program, "colorTex");
	uEmiTex = glGetUniformLocation(program, "emiTex");

	inPos = glGetAttribLocation(program, "inPos");
	inColor = glGetAttribLocation(program, "inColor");
	inNormal = glGetAttribLocation(program, "inNormal");
	inTexCoord = glGetAttribLocation(program, "inTexCoord");
}

void initObj() {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	if (inPos != -1) {
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(inPos, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inPos);
	}

	if (inColor != -1) {
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(inColor, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inColor);
	}

	if (inNormal != -1) {
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(inNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inNormal);
	}

	if (inTexCoord != -1) {
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 2,
			cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(inTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inTexCoord);
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		cubeNTriangleIndex * sizeof(unsigned int) * 3, cubeTriangleIndex,
		GL_STATIC_DRAW);

	model = glm::mat4(1.0f);

	colorTexId = loadTex("../img/color2.png");
	emiTexId = loadTex("../img/emissive.png");
}

GLuint loadShader(const char *fileName, GLenum type) {
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);

	//////////////////////////////////////////////
	// Creación y compilación del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, (const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete source;

	// Comprobamos que se compilo bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);

		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;

		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

unsigned int loadTex(const char *fileName) {
	unsigned char *map;
	unsigned int w, h;
	map = loadTexture(fileName, w, h);

	if (!map) {
		std::cout << "Error cargando el fichero: " << fileName << std::endl;
		exit(-1);
	}

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		(GLvoid *)map);
	delete[] map;
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);

	return texId;
}

void renderFunc() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/**/
	glUseProgram(program);

	// Texturas
	if (uColorTex != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTexId);
		glUniform1i(uColorTex, 0);
	}

	if (uEmiTex != -1) {
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, emiTexId);
		glUniform1i(uEmiTex, 1);
	}

	model = glm::mat4(2.0f);
	model[3].w = 1.0f;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	renderCube();

	std::srand(RAND_SEED);
	for (unsigned int i = 0; i < 10; i++) {
		float size = float(std::rand() % 3 + 1);

		glm::vec3 axis(glm::vec3(float(std::rand() % 2), float(std::rand() % 2),
			float(std::rand() % 2)));
		if (glm::all(glm::equal(axis, glm::vec3(0.0f))))
			axis = glm::vec3(1.0f);

		float trans = float(std::rand() % 7 + 3) * 1.00f + 0.5f;
		glm::vec3 transVec = axis * trans;
		transVec.x *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.y *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.z *= (std::rand() % 2) ? 1.0f : -1.0f;

		model = glm::rotate(glm::mat4(1.0f), angle * 2.0f * size, axis);
		model = glm::translate(model, transVec);
		model = glm::rotate(model, angle * 2.0f * size, axis);
		model = glm::scale(model, glm::vec3(1.0f / (size * 0.7f)));
		renderCube();
	}
	//*/
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (blendActive)
		glEnable(GL_BLEND);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_CONSTANT_COLOR, GL_CONSTANT_ALPHA);
	glBlendColor(RED, GREEN, BLUE, ALPHA);
	glBlendEquation(GL_FUNC_ADD);

	glUseProgram(postProccesProgram);

	if (uColorTexPP != -1) {
		glUniform1i(uColorTexPP, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
	}

	if (uVertexTexPP != -1) {
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, vertexBuffTexId);
		glUniform1i(uVertexTexPP, 1);
	}

	if (u_dofByCam != -1)
		glProgramUniform1i(postProccesProgram, u_dofByCam, dofByCam);
	else
		std::cout << "WARNING: u_dofByCam no definida" << std::endl;


	if (u_focal_distance != -1)
		glProgramUniform1f(postProccesProgram, u_focal_distance, focalDistance);
	else
		std::cout << "WARNING: u_focal_distance no definida" << std::endl;

	if (u_max_distance_factor != -1)
		glProgramUniform1f(postProccesProgram, u_max_distance_factor,
			maxDistanceFactor);
	else
		std::cout << "WARNING: u_max_distance_factor no definida" << std::endl;

	if (u_mask != -1)
		glProgramUniform1fv(postProccesProgram, u_mask, 25, mask[mask_index]);
	else
		std::cout << "WARNING: u_mask no definida" << std::endl;

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}

void renderCube() {
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));

	if (uModelViewMat != -1)
		glUniformMatrix4fv(uModelViewMat, 1, GL_FALSE, &(modelView[0][0]));
	if (uModelViewProjMat != -1)
		glUniformMatrix4fv(uModelViewProjMat, 1, GL_FALSE, &(modelViewProj[0][0]));
	if (uNormalMat != -1)
		glUniformMatrix4fv(uNormalMat, 1, GL_FALSE, &(normal[0][0]));

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3, GL_UNSIGNED_INT,
		(void *)0);
}

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------

void initShaderPP(const char *vname, const char *fname) {
	postProccesVShader = loadShader(vname, GL_VERTEX_SHADER);
	postProccesFShader = loadShader(fname, GL_FRAGMENT_SHADER);

	postProccesProgram = glCreateProgram();
	glAttachShader(postProccesProgram, postProccesVShader);
	glAttachShader(postProccesProgram, postProccesFShader);

	// glBindAttribLocation(postProccesProgram, 0, "inPos");

	glLinkProgram(postProccesProgram);
	int linked;
	glGetProgramiv(postProccesProgram, GL_LINK_STATUS, &linked);
	if (!linked) {
		// Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(postProccesProgram, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(postProccesProgram, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;
		glDeleteProgram(postProccesProgram);
		postProccesProgram = 0;
		exit(-1);
	}

	uColorTexPP = glGetUniformLocation(postProccesProgram, "colorTex");
	inPosPP = glGetAttribLocation(postProccesProgram, "inPos");
	uVertexTexPP = glGetUniformLocation(postProccesProgram, "vertexTex");
	// Para los ejercicios
	u_dofByCam = glGetUniformLocation(postProccesProgram, "dofByCam");
	u_focal_distance = glGetUniformLocation(postProccesProgram, "focalDistance");
	u_max_distance_factor = glGetUniformLocation(postProccesProgram, "maxDistanceFactor");
	u_mask = glGetUniformLocation(postProccesProgram, "mask");
}

void initPlane() {
	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);
	glGenBuffers(1, &planeVertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVertexVBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex * sizeof(float) * 3,
		planeVertexPos, GL_STATIC_DRAW);
	glVertexAttribPointer(inPosPP, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(inPosPP);
}

void initFBO() {
	glGenFramebuffers(1, &fbo);
	glGenTextures(1, &colorBuffTexId);
	glGenTextures(1, &depthBuffTexId);
	glGenTextures(1, &vertexBuffTexId);
}

void resizeFBO(unsigned int w, unsigned int h) {
	glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	glBindTexture(GL_TEXTURE_2D, vertexBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, depthBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		colorBuffTexId, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		depthBuffTexId, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D,
		vertexBuffTexId, 0);
	const GLenum buffs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, buffs);

	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER)) {
		std::cerr << "Error configurando el FBO" << std::endl;
		exit(-1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------

void resizeFunc(int width, int height) {
	glViewport(0, 0, width, height);
	proj = glm::perspective(glm::radians(60.0f), float(width) / float(height),
		1.0f, 50.0f);
	resizeFBO(width, height);
	glutPostRedisplay();
}

void idleFunc() {
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.02f;
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y) {
	std::cout << "KEY: " << key << std::endl << std::endl;
	const float walkSpeed = 0.15f;           // how fast we move
	const float rotSpeed = walkSpeed / 8.0f; // how fast we move
	float dx = 0.0f;                         // how much we strafe on X
	float dz = 0.0f;                         // how much we walk on Z

	switch (key) {
		// Movement

	case 'w':
		dz = -walkSpeed;
		break;
	case 's':
		dz = walkSpeed;
		break;
	case 'a':
		dx = -walkSpeed;
		break;
	case 'd':
		dx = walkSpeed;
		break;

		// Camera

	case 'l':
		pitch += rotSpeed;
		break;
	case 'j':
		pitch -= rotSpeed;
		break;
	case 'k':
		yaw += rotSpeed;
		break;
	case 'i':
		yaw -= rotSpeed;
		break;

		// Motion blur settings [P4.1]

	case 'R':
		RED += .1f;
		RED = clamp2(RED, 0.f, 1.f);
		break;
	case 'r':
		RED -= .1f;
		RED = clamp2(RED, 0.f, 1.f);
		break;
	case 'G':
		GREEN += .1f;
		GREEN = clamp2(GREEN, 0.f, 1.f);
		break;
	case 'g':
		GREEN -= .1f;
		GREEN = clamp2(GREEN, 0.f, 1.f);
		break;
	case 'B':
		BLUE += .1f;
		BLUE = clamp2(BLUE, 0.f, 1.f);
		break;
	case 'b':
		BLUE -= .1f;
		BLUE = clamp2(BLUE, 0.f, 1.f);
		break;
	case 'P':
		ALPHA += .1f;
		ALPHA = clamp2(ALPHA, 0.f, 1.f);
		break;
	case 'p':
		ALPHA -= .1f;
		ALPHA = clamp2(ALPHA, 0.f, 1.f);
		break;

		// DOF por teclado [P4.2]
	case 'F':
		focalDistance += 1.f;
		focalDistance = clamp2(focalDistance, -30.f, 5.f);
		std::cout << "byKbDOF = " << focalDistance << std::endl;
		break;
	case 'f':
		focalDistance -= 1.f;
		focalDistance = clamp2(focalDistance, -30.f, 0.f);
		std::cout << "byKbDOF = " << focalDistance << std::endl;
		break;
	case 'M':
		maxDistance += 1.f;
		maxDistance = clamp2(maxDistance, 1.f, 10.f);
		maxDistanceFactor = 1 / maxDistance;
		break;
	case 'm':
		maxDistance -= 1.f;
		maxDistance = clamp2(maxDistance, 1.f, 10.f);
		maxDistanceFactor = 1 / maxDistance;
		break;

		// Mascara de convolucion
	case '1':
		mask_index = 0;
		break;
	case '2':
		mask_index = 1;
		break;
	case '3':
		mask_index = 2;
		break;
	case '4':
		mask_index = 3;
		break;

		// Toggle DOF by camera
	case 'c':
	case 'C':
		DOF_by_camera = !DOF_by_camera;
		if (!DOF_by_camera) {
			dofByCam = 0;
			focalDistance = auxFocalDistance;
			std::cout << "Now DOF is managed by Keyboard F/f" << std::endl;
		}
		if (DOF_by_camera) {
			dofByCam = 1;
			auxFocalDistance = focalDistance;
			std::cout << "Now DOF is managed by camera distance" << std::endl;
		}
		break;

		// Toggle blend
	case '0':
		blendActive = !blendActive;

		// Default
	default: break;
	}

	glm::vec3 forward(view[0][2], view[1][2], view[2][2]); // Z
	glm::vec3 strafe(view[0][0], view[1][0], view[2][0]);  // X
	eyeVec += (dz * forward + dx * strafe);
	UpdateView();
}

// MOUSE BUTTONS INPUT ACTIONS
void mouseFunc(int button, int state, int x, int y)
{
	if (state == 0) { std::cout << "MOUSE BUTTON PRESSED: "; }
	else { std::cout << "MOUSE BUTTON RELEASED: "; }
	if (button == 0) std::cout << "Left" << std::endl;
	if (button == 1) std::cout << "Center" << std::endl;
	if (button == 2) std::cout << "Right" << std::endl;
	std::cout << "AT (" << x << ", " << y << ")" << std::endl << std::endl;

	if (state == 0 && button == 0) // left click enter
	{
		mouseX = (float)x;
		mouseY = (float)y;
	}
}

// MOUSE DISPLACEMENT INPUT ACTIONS - [ P1.O1 ]
void mouseMotionFunc(int x, int y)
{
	pitch += (mouseX - (float)x) * 0.0001f;
	yaw += (mouseY - (float)y) * 0.0001f;
	UpdateView();
}

// UPDATE CORRECTLY - Fix camera bug of lose Z axis
void UpdateView() {
	glm::mat4 matYaw = glm::mat4(1.0f);   // identity matrix
	glm::mat4 matPitch = glm::mat4(1.0f); // identity matrix

	// Apply acumulated angles
	matYaw = glm::rotate(matYaw, yaw, glm::vec3(-1.0f, 0.0f, 0.0f));
	matPitch = glm::rotate(matPitch, pitch, glm::vec3(0.0f, 1.0f, 0.0f));

	// Calc roration
	glm::mat4 rotate = glm::mat4(1.0f);
	rotate = matYaw * matPitch;

	// Calc translation
	glm::mat4 translate = glm::mat4(1.0f);
	translate[3].z = -25.0f;
	translate = glm::translate(translate, -eyeVec);

	// New view will be the product of two previous matrix
	view = rotate * translate;
}

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------

template <typename t>
t clamp2(t x, t min, t max) {
	if (x < min) x = min;
	if (x > max) x = max;
	return x;
}

